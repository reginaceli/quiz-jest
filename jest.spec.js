const request = require('supertest'); 
 
describe.only("Name of the group", () => { 
  
  // POST
  it.only("POST", async () => { 
    const resp = await 
request("https://reqres.in/api/users").post( 
  "users" 
  ); 
  expect(resp.statusCode).toBe(201); 
  });

// PUT
it.only("PUT", async () => { 
  const resp = await 
request("https://reqres.in/api/users/2").put( 
"users" 
); 
expect(resp.statusCode).toBe(200); 
});

// GET
it.only("GET", async () => { 
  const resp = await 
request("https://reqres.in/api/users?page=2").get( 
"users" 
); 
expect(resp.statusCode).toBe(200); 
});

// DELETE
it.only("DELETE", async () => { 
  const resp = await 
request("https://reqres.in/api/users/2").delete( 
"users" 
); 
expect(resp.statusCode).toBe(204); 
});
});